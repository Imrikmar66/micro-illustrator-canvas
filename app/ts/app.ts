import '../scss/styles';
import {App} from './classes/App';
import { Canvas } from './classes/Canvas';
import EventManager from './classes/EventManager';
import { Drawer } from './classes/Drawer';
import { Translater } from './classes/Translater';
import { Selector } from './classes/Selector';
import ServiceManager from './classes/ServiceManager';
import saver from './classes/Saver';
import { Remover } from './classes/Remover';
import copier from './classes/Copier';


(async () => {

    const app = new App(new Canvas('canvas'));
    const drawer = new Drawer('drawer');
    const translater = new Translater('translater');
    const selector = new Selector('selector');
    const remover = new Remover('remove');
    copier;

    ServiceManager.add('app', app);
    ServiceManager.add('drawer', drawer);
    ServiceManager.add('translater', translater);
    ServiceManager.add('selector', selector);
    ServiceManager.add('remover', remover);

    app.init();

    EventManager.on('grabbable', null, (initier: any) => {
        app.canvas.$canvas.classList.add('grab');
    });
    EventManager.on('cancel actions', null, (initier: any) => {
        app.canvas.$canvas.classList.remove('grab');
    });

    document.getElementById('plus')?.addEventListener('click', e => {
        app.canvas.zoom();
    })
    document.getElementById('moins')?.addEventListener('click', e => {
        app.canvas.unZoom();
    });

})();