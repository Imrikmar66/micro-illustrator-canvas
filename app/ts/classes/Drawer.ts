import { Point } from "./Point";
import { Zone } from "./Zone";
import { Canvas } from "./Canvas";
import eventManager from "./EventManager";
import { AppService } from "./AppService";
import ServiceManager from "./ServiceManager";
import { App } from "./App";
import { Color } from "./Color";

export class Drawer implements AppService {

    private $drawer: HTMLElement;
    private is_drawing: boolean = false;
    private points: Point[] = [];

    constructor(id: string) {
        this.$drawer = <HTMLElement> document.getElementById(id);
        this.bindEvents();
    }

    private bindEvents() {
        this.$drawer.addEventListener('click', e => {

            if(!this.isDrawing()){
                eventManager.trigger('cancel actions', this);
                this.start();
            }
            else
                this.end();
    
        });

        eventManager.on('cancel actions', this, (initier:any) => {
            if(initier != this) {
                this.end();
            }
        });
    }

    start() {
        this.is_drawing = true;
        this.$drawer.classList.add('drawing');
    }

    end() {
        this.points = [];
        this.is_drawing = false;
        this.$drawer.classList.remove('drawing');
    }

    getPoints() {
        return this.points;
    }

    isDrawing() {
        return this.is_drawing;
    }

    min3() {
        return this.points.length > 2;
    }

    hit(x: number, y: number) {

        const app = <App> ServiceManager.get('app');

        if(!this.points.length) return false;

        const firstPoint = this.points[0];
        return Math.sqrt((x-firstPoint.x) ** 2 + (y - firstPoint.y) ** 2) < (app.canvas.pointRange/app.canvas.currentZoom);

    }

    addPoint( x: number, y: number ) {
        this.points.push(new Point(x, y));
    }

    createZone() {
        return new Zone(new Color, this.points);
    }

    draw() {
        const app = <App> ServiceManager.get('app');
        const canvas = app.canvas;
        const mouse = app.mouse;
        if(this.isDrawing()){

            const firstPoint = this.points[0];
            if(firstPoint) {
                const ctx = canvas.ctx;
                ctx.beginPath();
                ctx.lineWidth = 1/canvas.currentZoom;
                ctx.strokeStyle = 'red';
                ctx.moveTo(firstPoint.x, firstPoint.y);
                for(let point of this.points){
                    ctx.lineTo(point.x , point.y);
                }
                ctx.lineTo(mouse.x, mouse.y);
                ctx.stroke();
                ctx.closePath();

                if(this.hit(mouse.x, mouse.y) && this.min3()){
                    ctx.beginPath();
                    ctx.arc(firstPoint.x, firstPoint.y, 5/canvas.currentZoom, 0, 2 * Math.PI);
                    ctx.strokeStyle = 'green';
                    ctx.stroke();
                    ctx.closePath();
                }
            }

        }
    }

}