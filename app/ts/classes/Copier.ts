import ServiceManager from "./ServiceManager";
import { App } from "./App";
import { Color } from "./Color";
import KeyBoard from "./KeyBoard";

class Copier {

    toPaste: Color|null;

    constructor() {
        this.bindEvents();
    }

    private bindEvents() {
        const vKey = 86,
            cKey = 67;

        document.addEventListener('keydown', e => {
            const app = <App> ServiceManager.get('app');
            if (KeyBoard.ctrlDown && (e.keyCode == cKey) && app.selected) this.toPaste = app.selected.color;
            if (KeyBoard.ctrlDown && (e.keyCode == vKey) && app.selected && this.toPaste) {
                app.selected.color = this.toPaste.clone();
                app.selected.color.createPicker(app.$picker);
            }
        })
    }

}
export default new Copier;