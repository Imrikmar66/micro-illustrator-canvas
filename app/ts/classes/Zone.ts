import { Color, ColorLiteral } from "./Color";
import { Point, PointLiteral } from "./Point";
import { Tools } from "./Tools";
import { Canvas } from "./Canvas";

export class Zone {

    uniqid: string;
    points: Point[];
    color: Color;

    constructor(color: Color, points: Point[] = []) {
        this.uniqid = Tools.uniqId();
        this.color = color;
        this.points = points;
    }

    addPoint( x: number, y: number ) {
        this.points.push(new Point(x, y));
    }

    draw( canvas: Canvas, current: boolean ) {

        const ctx = canvas.ctx;
        ctx.beginPath();
        for(let key in this.points){
            const point = this.points[key];
            if(parseInt(key) === 0){
                ctx.moveTo(point.x, point.y);
            }
            else {
                ctx.lineTo(point.x, point.y);
            }
            
        }
        if(this.points[0]){
            ctx.lineTo(this.points[0].x, this.points[0].y);
        }
        //@ts-ignore
        ctx.addHitRegion({id: this.uniqid});
        if(current) {
            ctx.lineWidth = 2;
            ctx.strokeStyle = '#000';
            ctx.stroke();
        }
        ctx.closePath();
        ctx.fillStyle = this.color.readPicker().toRGBA();
        ctx.fill();

        if(current)
            this.color.managePicker();
    }

}

export interface ZoneLiteral {
    points: PointLiteral[];
    color: ColorLiteral;
}