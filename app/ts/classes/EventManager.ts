class EventManager {

    private events: { [key: string]: { sender: any, fn: Function}[] } = {};

    on(event: string, sender: any, fn: Function) {
        if(!this.events[event])
            this.events[event] =  [];
        this.events[event].push({sender, fn});
    }

    trigger( event: string, sender: any ) {

        for(let handler of this.events[event]) {
            handler.fn(sender);
        }

    }

}

export default new EventManager;