import eventManager from "./EventManager";
import { AppService } from "./AppService";

export class Selector implements AppService {

    private $selector: HTMLElement;
    private is_selected: boolean = false;

    constructor(id: string) {
        this.$selector = <HTMLElement> document.getElementById(id);
        this.bindEvents();
    }

    private bindEvents() {
        this.$selector.addEventListener('click', e => {

            if(!this.is_selected){
                eventManager.trigger('cancel actions', this);
                this.start();
            }
            else
                this.end();
    
        });

        eventManager.on('cancel actions', this, (initier:any) => {
            if(initier != this)
                this.end();
        });
    }

    start() {
        this.is_selected = true;
        this.$selector.classList.add('selecting');
    }

    end() {
        this.is_selected = false;
        this.$selector.classList.remove('selecting');
    }

}