import eventManager from "./EventManager";
import { AppService } from "./AppService";
import ServiceManager from "./ServiceManager";
import { App } from "./App";
import { Zone } from "./Zone";

export class Remover implements AppService {

    private $selector: HTMLElement;
    private is_removing: boolean = false;

    constructor(id: string) {
        this.$selector = <HTMLElement> document.getElementById(id);
        this.bindEvents();
    }

    private bindEvents() {
        this.$selector.addEventListener('mousedown', e => {

            this.is_removing = true;
            this.$selector.classList.add('removing');
    
        });
        this.$selector.addEventListener('mouseup', e => {

            if(this.is_removing) {
                this.is_removing = false;
                this.$selector.classList.remove('removing');
                const app = <App> ServiceManager.get('app');
                if(app.selected)
                    delete app.zones[(app.selected).uniqid];
            }
    
        });

        document.addEventListener('keydown', e => {
            if(e.keyCode == 46) {
                const app = <App> ServiceManager.get('app');
                if(app.selected)
                    this.$selector.classList.add('removing');
            }
        })
        document.addEventListener('keyup', e => {
            if(e.keyCode == 46) {
                const app = <App> ServiceManager.get('app');
                this.$selector.classList.remove('removing');
                if(app.selected){
                    delete app.zones[(app.selected).uniqid];
                    app.$picker.innerHTML = '';
                }
            }
        })

        eventManager.on('cancel actions', this, (initier:any) => {
            this.$selector.classList.remove('removing');
        });

    }

}