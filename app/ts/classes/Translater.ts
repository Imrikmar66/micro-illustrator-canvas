import eventManager from "./EventManager";
import { AppService } from "./AppService";

export class Translater implements AppService {

    private $translater: HTMLElement;
    private is_translating: boolean = false;

    constructor(id: string) {
        this.$translater = <HTMLElement> document.getElementById(id);
        this.bindEvents();
    }

    private bindEvents() {
        this.$translater.addEventListener('click', e => {

            if(!this.is_translating){
                eventManager.trigger('cancel actions', this);
                eventManager.trigger('grabbable', this);
                this.start();
            }
            else
                this.end();
    
        });

        eventManager.on('cancel actions', this, (initier:any) => {
            if(initier != this) {
                this.end();
            }
        });
    }

    start() {
        this.is_translating = true;
        this.$translater.classList.add('translating');
    }

    end() {
        this.is_translating = false;
        this.$translater.classList.remove('translating');
    }

    isTranslating() {
        return this.is_translating;
    }
}