import { AppService } from "./AppService";

class ServiceManager {

    private services: {[key: string]: AppService} = {};

    add( key: string, service: AppService ) {
        this.services[key] = service;
    }

    get( key: string ) {
        let service = null;
        if(service = this.services[key])
            return this.services[key];

        throw new Error('Service does not exist');
    }

}

export default new ServiceManager;