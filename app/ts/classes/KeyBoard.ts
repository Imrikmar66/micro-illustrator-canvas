import { App } from "./App";
import ServiceManager from "./ServiceManager";
import EventManager from "./EventManager";

class Keyboard {

    ctrlDown: boolean = false;

    constructor() {
        this.bindEvents();
    }

    private bindEvents() {

        const ctrlKey = 17,
            cmdKey = 91;
        
        document.addEventListener('keydown', e => {
            if (e.keyCode == ctrlKey || e.keyCode == cmdKey) this.ctrlDown = true;
        })
        document.addEventListener('keyup', e => {
            if (e.keyCode == ctrlKey || e.keyCode == cmdKey) this.ctrlDown = false;
        })

        document.addEventListener('keydown', e => {
            if(e.keyCode == 27) EventManager.trigger('cancel actions', null);
        })
    }

}

export default new Keyboard