import {Canvas} from './Canvas';
import { Point } from './Point';
import { Color } from './Color';
import { Zone, ZoneLiteral } from './Zone';
import { AppService } from './AppService';
import ServiceManager from './ServiceManager';
import { Translater } from './Translater';
import { Drawer } from './Drawer';
import saver from './Saver';
export class App implements AppService {

    $picker: HTMLElement;
    canvas: Canvas;
    mouse: Point;
    color: Color;
    zones: {[key: string]: Zone} = {};
    selected: Zone|null;

    img: HTMLImageElement;

    constructor( canvas: Canvas ) {
        this.$picker = <HTMLElement>document.getElementById('picker');
        this.canvas = canvas;
        this.mouse = new Point(0, 0);
    }

    init() {

        let src = saver.get<string>('img');
        if(src){
            this.img = new Image;
            this.img.src = src;
            this.img.onload = () => this.draw();
        }

        let zonesjson: {[key: string]: ZoneLiteral} = {};
        if(zonesjson = saver.get<{[key: string]: ZoneLiteral}>('zones', true)) {
            for(let zonekey in zonesjson) {
                const zonejson = zonesjson[zonekey];
                const points = [];
                for(let jsonpoint of zonejson.points) {
                    points.push(new Point(jsonpoint.x, jsonpoint.y))
                }
                const color = new Color(zonejson.color.r, zonejson.color.g, zonejson.color.b, zonejson.color.a);
                const zone = new Zone(color, points);
                this.zones[zone.uniqid] = zone;
            }
        }
        

        const $uploader = <HTMLInputElement> document.getElementById('upload-img');
        $uploader.addEventListener('change', e => {
    
            const reader = new FileReader;
    
            reader.addEventListener('load', e => {

                const url = <string>reader.result;
                this.img = new Image;
                this.img.src = url;
                this.img.onload = () => this.draw();
            });
    
            let blob = null;
            if($uploader.files && (blob = $uploader.files[0])){
                reader.readAsDataURL(blob);
            }
    
        });

        let currentlyMouseDown = false
        this.canvas.$canvas.addEventListener('mousedown', e => {
            currentlyMouseDown = true;
        });
        this.canvas.$canvas.addEventListener('mouseup', e => {
            currentlyMouseDown = false;
        });

        const translater = <Translater> ServiceManager.get('translater');
        this.canvas.$canvas.addEventListener('mousemove', e => {
            
            if(translater.isTranslating() && currentlyMouseDown) {
                const decalX = (e.x - this.canvas.$canvas.offsetLeft - (this.canvas.currentTranslation.x*this.canvas.currentZoom)) / this.canvas.currentZoom;
                const decalY = (e.y - this.canvas.$canvas.offsetTop - (this.canvas.currentTranslation.y*this.canvas.currentZoom)) / this.canvas.currentZoom;
                const translate = new Point(
                    decalX - this.mouse.x,
                    decalY - this.mouse.y
                );
                this.canvas.translate(translate); 
            }

            const x = (e.x - this.canvas.$canvas.offsetLeft - (this.canvas.currentTranslation.x*this.canvas.currentZoom)) / this.canvas.currentZoom;
            const y = (e.y - this.canvas.$canvas.offsetTop - (this.canvas.currentTranslation.y*this.canvas.currentZoom)) / this.canvas.currentZoom;

            this.mouse = new Point(x, y);
        });

        const drawer = <Drawer> ServiceManager.get('drawer');
        this.canvas.$canvas.addEventListener('click', e => {
            //@ts-ignore
            if (e.region) {
                //@ts-ignore
                this.zones[e.region].color.createPicker(this.$picker);
                //@ts-ignore
                this.selected = this.zones[e.region];
            }
            else if(!translater.isTranslating()) {
                this.selected = null;
                this.$picker.innerHTML = '';
            }
            if(drawer.isDrawing()) {
                const x = (e.x - this.canvas.$canvas.offsetLeft - (this.canvas.currentTranslation.x*this.canvas.currentZoom)) / this.canvas.currentZoom;
                const y = (e.y - this.canvas.$canvas.offsetTop - (this.canvas.currentTranslation.y*this.canvas.currentZoom)) / this.canvas.currentZoom;
                drawer.addPoint(x, y);
                if(drawer.hit(x, y) && drawer.min3()){
                    const zone = drawer.createZone();
                    zone.color.createPicker(this.$picker);
                    this.zones[zone.uniqid] = zone;
                    this.selected = zone;
                    drawer.end();
                    drawer.start();
                }
            }
        });

    }

    draw() {
        this.canvas.clear();

        const ctx = this.canvas.ctx;
        ctx.drawImage(this.img, 0, 0);

        ctx.fillStyle = "black";
        const fontsize = 10/this.canvas.currentZoom;
        ctx.font = `${fontsize}px Arial`;
        ctx.fillText(`x: ${this.mouse.x}, y: ${this.mouse.y}`, this.mouse.x, this.mouse.y - (10/this.canvas.currentZoom));
        
        for(let key in this.zones) {
            const zone = this.zones[key];
            zone.draw(this.canvas, this.selected === zone);
        }

        const drawer = <Drawer> ServiceManager.get('drawer');
        drawer.draw();

        requestAnimationFrame(() => this.draw());
    }
    
    
}