export interface Picker {

    $ctn: HTMLElement
    r: HTMLInputElement
    g: HTMLInputElement
    b: HTMLInputElement
    a:HTMLInputElement
    sr: HTMLSpanElement
    sg: HTMLSpanElement
    sb: HTMLSpanElement
    sa:HTMLSpanElement

}