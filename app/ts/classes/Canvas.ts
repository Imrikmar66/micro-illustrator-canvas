import { Point } from "./Point";

export class Canvas {

    public $canvas: HTMLCanvasElement;
    public ctx: CanvasRenderingContext2D;
    public currentZoom: number ;
    public currentTranslation: Point;
    public pointRange: number = 5;

    constructor(id: string) {

        this.currentZoom = 1;
        this.currentTranslation = new Point(0, 0);

        this.$canvas = document.getElementById(id) as HTMLCanvasElement;
        this.resizeCanvas();

        this.ctx = this.$canvas.getContext('2d') as CanvasRenderingContext2D;

        this.bindEvents();
    }

    private bindEvents() {
        window.addEventListener('resize', e => this.resizeCanvas())
    }

    private resizeCanvas(): void {
        
        this.$canvas.height = window.innerHeight;
        this.$canvas.width = window.innerWidth;

    }

    translate(point: Point) {
        this.currentTranslation = new Point(
            this.currentTranslation.x + point.x,
            this.currentTranslation.y + point.y
        );
        this.ctx.translate(point.x, point.y);
    }

    resetTranslate() {
        this.translate(new Point(
            -this.currentTranslation.x,
            -this.currentTranslation.y
        ));
    }

    public zoom() {
        this.resetTranslate();
        this.ctx.scale(2, 2);
        this.currentZoom*=2;
    }

    public unZoom() {
        if(this.currentZoom > 1){
            this.resetTranslate();
            this.currentZoom/=2;
            this.ctx.scale(0.5, 0.5);
        }
    }

    public clear(): void {
        this.ctx.clearRect( 
            -this.currentTranslation.x, 
            -this.currentTranslation.y, 
            this.$canvas.width, 
            this.$canvas.height 
        );
    }

}