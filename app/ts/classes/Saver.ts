import { AppService } from "./AppService";
import { App } from "./App";
import ServiceManager from "./ServiceManager";

class Saver implements AppService {

    constructor() {
        this.bindEvents();
    }
    
    private bindEvents() {
        window.addEventListener("beforeunload", e => {
            const app = <App> ServiceManager.get('app');
            this.add('zones', app.zones);
            this.add('img', app.img.src);
        }, false);
    }

    add(key: string, value: string|Object) {

        if( typeof value == 'object' ){
            value = JSON.stringify(value);
        }

        localStorage.setItem(key, <string>value);

    }

    get<T>(key: string, object = false): T {

        let value = localStorage.getItem(key);
        if(value && object) {
            return <T>JSON.parse(value);
        }

        return <T><any>value;

    }

}

export default new Saver;