import { Picker } from "./Picker";

export class Color {

    private r: number;
    private g: number;
    private b: number;
    private a: number;

    private picker: Picker;

    constructor( r = 0, g = 0, b = 0, a = 0.5) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    clone() {
        return new Color(this.r, this.g, this.b, this.a);
    }

    toRGBA() {
        return `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`;
    }

    createPicker( $ctn: HTMLElement ) {

        const r = document.createElement('input');
        r.setAttribute('type', 'range');
        r.setAttribute('min', '0');
        r.setAttribute('max', '255');
        r.setAttribute('value', ''+this.r);

        const g = document.createElement('input');
        g.setAttribute('type', 'range');
        g.setAttribute('min', '0');
        g.setAttribute('max', '255');
        g.setAttribute('value', ''+this.g);

        const b = document.createElement('input');
        b.setAttribute('type', 'range');
        b.setAttribute('min', '0');
        b.setAttribute('max', '255');
        b.setAttribute('value', ''+this.b);

        const a = document.createElement('input');
        a.setAttribute('type', 'range');
        a.setAttribute('min', '0');
        a.setAttribute('max', '1');
        a.setAttribute('step', '0.01');
        a.setAttribute('value', ''+this.a);

        const lr = document.createElement('label'), sr = document.createElement('span');
        lr.append(sr, r);
        const lg = document.createElement('label'), sg = document.createElement('span');
        lg.append(sg, g);
        const lb = document.createElement('label'), sb = document.createElement('span');
        lb.append(sb, b);
        const la = document.createElement('label'), sa = document.createElement('span');
        la.append(sa, a);

        this.picker = { $ctn, r, g, b, a, sr, sg, sb, sa };

        $ctn.innerHTML = '';
        $ctn.append(
            lr,
            lg,
            lb,
            la,
        );
        
    }

    managePicker() {
        (<HTMLElement>(this.picker.$ctn.parentElement)).style.backgroundColor = this.toRGBA();
    }

    readPicker() {
        if(this.picker) {
            this.r = parseInt(this.picker.r.value);
            this.g = parseInt(this.picker.g.value);
            this.b = parseInt(this.picker.b.value);
            this.a = parseFloat(this.picker.a.value);
            this.picker.sr.innerHTML = ''+this.r;
            this.picker.sg.innerHTML = ''+this.g;
            this.picker.sb.innerHTML = ''+this.b;
            this.picker.sa.innerHTML = ''+this.a;
        }

        return this;
    }

    toJSON() {
        return {
            r: this.r,
            g: this.g,
            b: this.b,
            a: this.a
        }
    }

}

export interface ColorLiteral {
    r: number;
    g: number;
    b: number;
    a: number;
}